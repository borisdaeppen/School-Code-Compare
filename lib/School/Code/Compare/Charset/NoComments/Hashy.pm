package School::Code::Compare::Charset::NoComments::Hashy;
# ABSTRACT: trim comments

use strict;
use warnings;

sub new {
    my $class = shift;

    my $self = {
               };
    bless $self, $class;

    return $self;
}

sub filter {
    my $self      = shift;
    my $lines_ref = shift;

    my @no_comments;

    foreach my $row (@{$lines_ref}) {
      next if ($row =~ /^#/);
      $row = $1 if ($row =~ /(.*)#.*/);

      push @no_comments, $row;
    }

    return \@no_comments;
}

1;
