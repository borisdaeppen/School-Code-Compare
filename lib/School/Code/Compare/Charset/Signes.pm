package School::Code::Compare::Charset::Signes;
# ABSTRACT: remove word characters completely

use strict;
use warnings;

sub new {
    my $class = shift;

    my $self = {
               };
    bless $self, $class;

    return $self;
}

sub filter {
    my $self   = shift;
    my $lines_ref = shift;

    my @signes;

    foreach my $row (@{$lines_ref}) {

      $row =~ s/\w//g;
      next if ($row eq '');

      push @signes, $row;
    }

    return \@signes;
}

1;
