package School::Code::Compare::Charset::NumSignes;
# ABSTRACT: collapse words and numbers into abstract placeholders

use strict;
use warnings;

sub new {
    my $class = shift;

    my $self = {
               };
    bless $self, $class;

    return $self;
}

sub filter {
    my $self   = shift;
    my $lines_ref = shift;

    my @numsignes;

    foreach my $row (@{$lines_ref}) {

        #$row =~ s/[:alpha:]/a/g;
      $row =~ s/[[:alpha:]]+/a/g;
      $row =~ s/\d+/0/g;
      next if ($row eq '');

      push @numsignes, $row;
    }

    return \@numsignes;
}

1;
