package School::Code::Compare::Charset::NoWhitespace;
# ABSTRACT: trim whitespace since it's mostly irrelevant

use strict;
use warnings;

sub new {
    my $class = shift;

    my $self = {
               };
    bless $self, $class;

    return $self;
}

sub filter {
    my $self      = shift;
    my $lines_ref = shift;

    my @no_whitespace;

    foreach my $row (@{$lines_ref}) {
      $row =~ s/\s*//g;
      next if ($row eq '');
      push @no_whitespace, $row;
    }

    return \@no_whitespace;
}

1;
